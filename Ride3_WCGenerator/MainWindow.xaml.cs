﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Drawing;
using System.IO;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;

namespace Ride3_WCGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region VARS

        private const string dateSeparator = " - ";
        private const string bkgFolderName = "Background";
        private const string infoBoxFolderName = "InfoBox";
        private const string trackFolderName = "Track";
        private const string lightCondFolderName = "LightCondition";

        private static readonly List<string> inputImgExtList = new List<string>() { ".png", ".PNG", ".jpeg", ".JPEG" };
        private static readonly StringFormat centerFormat = new StringFormat()
        {
            Alignment = StringAlignment.Center,
            LineAlignment = StringAlignment.Near
        };

        private static IDictionary<string, Image> infoBoxImgMap = new Dictionary<string, Image>(StringComparer.OrdinalIgnoreCase);
        private static IDictionary<string, Image> trackImgMap = new Dictionary<string, Image>(StringComparer.OrdinalIgnoreCase);
        private static IDictionary<string, Image> lightConditionImgMap = new Dictionary<string, Image>(StringComparer.OrdinalIgnoreCase);
        private static Image bugImage = null, dateImage = null, timeImage = null;

        private static string excelFilePath, inputFolderPath, outputFolderPath;
        private static List<WCExcelData> excelDataList = new List<WCExcelData>();

        private static TextPrinterParams infoBoxTitleParams = new TextPrinterParams(),
                                         infoBoxCommonParams = new TextPrinterParams(),
                                         bikeConstructorParams = new TextPrinterParams(),
                                         bikeDetailsParams = new TextPrinterParams();

        private static int infoBoxTextsMargin, bikeTextsMargin;
        private static bool isInfoBoxTitleUppercase, isInfoBoxCommonUppercase, isBikeConstructorUppercase, isBikeDetailsUppercase;
        private bool bkgTaskWorking = false;

        private static AboutWindow aboutWindow = null;

        #endregion VARS

        public MainWindow()
        {
            InitializeComponent();
            SetUIConfiguration();
            using (MemoryStream outStream = new MemoryStream())
            {
                PngBitmapEncoder enc = new PngBitmapEncoder();
                BitmapImage bitmapImage = new BitmapImage(
                                        new Uri("pack://application:,,,/res/Bug.png", UriKind.Absolute));
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                bugImage = new Bitmap(outStream);
            }   
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if(aboutWindow != null)
            {
                aboutWindow.Visibility = Visibility.Hidden;
                aboutWindow.Close();
            }
            base.OnClosing(e);
        }

        private async void StartButton_Click(object sender, RoutedEventArgs e)
        {
            StopButton.IsEnabled = true;
            StartButton.IsEnabled = false;
            bkgTaskWorking = true;

            Stopwatch stopwatch = Stopwatch.StartNew();

            if (!GetUIConfiguration())
            {
                ConsoleText.AppendText(String.Format("{0}ERROR, please check UI settings!", Environment.NewLine));
                ConsoleText.ScrollToEnd();

                stopwatch.Stop();
                bkgTaskWorking = false;
                return;
            }

            if(!LoadData())
            {
                stopwatch.Stop();
                bkgTaskWorking = false;
                StopButton.IsEnabled = false;
                StartButton.IsEnabled = true;
                return;
            }

            // Run a method in another thread
            await Task.Run(() => BkgTask());

            // Task finished
            bkgTaskWorking = false;

            stopwatch.Stop();
            ConsoleText.AppendText(String.Format("{0}Elapsed time: {1} seconds", Environment.NewLine, stopwatch.Elapsed.ToString(@"ss\.fff")));
            ConsoleText.ScrollToEnd();

            StopButton.IsEnabled = false;
            StartButton.IsEnabled = true;
        }

        // This is a thread-safe method. You can run it in any thread
        internal void BkgTask()
        {
            while (bkgTaskWorking)
            {
                foreach (WCExcelData excelData in excelDataList)
                {
                    if (!bkgTaskWorking) { break; }
                    else
                    {
                        Image bkgImage = null;

                        string imgPath = $"{inputFolderPath}\\{bkgFolderName}\\{excelData.number.ToString().Trim()}";
                        FindImageWithAnyExtension(ref bkgImage, imgPath);

                        if (bkgTaskWorking)
                        {
                            if (bkgImage != null)
                            {
                                Bitmap bmp = (Bitmap)bkgImage;
                                CreateWCPoster(bmp, excelData);
                                bmp?.Dispose();
                            }
                            else
                            {
                                ConsoleText.Dispatcher.Invoke(() =>
                                {
                                    ConsoleText.AppendText(String.Format("{0}ERROR: background not found for WC#{1}", Environment.NewLine, excelData.number.ToString()));
                                    ConsoleText.ScrollToEnd();
                                });
                            }
                        }

                        bkgImage?.Dispose();
                    }
                }
                bkgTaskWorking = false;
            }
        }

        private static bool FindImageWithAnyExtension(ref Image image, string imgPath)
        {
            foreach (string ext in inputImgExtList)
            {
                try
                {
                    image = Image.FromFile($"{imgPath}{ext}");
                    break;
                }
                catch (FileNotFoundException)
                { /* Do nothing, continue to next extension */ }
            }

            return (image != null);
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            StopButton.IsEnabled = false;
            bkgTaskWorking = false;
            ConsoleText.AppendText(String.Format("{0}Operation stopped by user", Environment.NewLine));
            ConsoleText.ScrollToEnd();
            StartButton.IsEnabled = true;
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            if (aboutWindow == null)
            {
                aboutWindow = new AboutWindow();
                aboutWindow.Show();
            }
            else if (aboutWindow.Visibility == Visibility.Hidden)
            {
                aboutWindow.Show();
            }
            else
            {
                aboutWindow.Focus();
            }
        }

        private bool GetUIConfiguration()
        {
            bool isValid = true;

            string str = ExcelPicker.GetPath().Trim(' ');
            if (isValid && str.Length > 0) { Properties.Settings.Default.ExcelFilePath = str; }
            else { isValid = false; }

            str = InputFolderPicker.GetPath().Trim(' ');
            if (isValid && str.Length > 0) { Properties.Settings.Default.InputFolderPath = str; }
            else { isValid = false; }

            str = OutputFolderPicker.GetPath().Trim(' ');
            if (isValid && str.Length > 0) { Properties.Settings.Default.OutputFolderPath = str; }
            else { isValid = false; }

            // Fonts
            Properties.Settings.Default.InfoBoxTitleFont = InfoBoxTitleTextConfig.Font;
            Properties.Settings.Default.InfoBoxCommonFont = InfoBoxCommonTextConfig.Font;
            Properties.Settings.Default.ConstructorFont = ConstructorTextConfig.Font;
            Properties.Settings.Default.BikeFont = BikeTextConfig.Font;

            // Colors
            Properties.Settings.Default.InfoBoxTitleColor = InfoBoxTitleTextConfig.GetColor();
            Properties.Settings.Default.InfoBoxCommonColor = InfoBoxCommonTextConfig.GetColor();
            Properties.Settings.Default.ConstructorColor = ConstructorTextConfig.GetColor();
            Properties.Settings.Default.BikeColor = BikeTextConfig.GetColor();

            // Uppercase
            Properties.Settings.Default.InfoBoxTitleUpper = InfoBoxTitleTextConfig.GetUpperCase();
            Properties.Settings.Default.InfoBoxCommonUpper = InfoBoxCommonTextConfig.GetUpperCase();
            Properties.Settings.Default.ConstructorUpper = ConstructorTextConfig.GetUpperCase();
            Properties.Settings.Default.BikeUpper = BikeTextConfig.GetUpperCase();

            //Coordinates
            int tempCoordX = 0, tempCoordY = 0;

            isValid &= SaveCoordToSetting(ref X_InfoBox, ref tempCoordX);
            isValid &= SaveCoordToSetting(ref Y_InfoBox, ref tempCoordY);
            Properties.Settings.Default.InfoBoxPos = new System.Drawing.Point(tempCoordX, tempCoordY);

            isValid &= SaveCoordToSetting(ref HMargin_InfoBox, ref tempCoordX);
            Properties.Settings.Default.InfoBoxTextsMargin = tempCoordX;

            isValid &= SaveCoordToSetting(ref Y_Title, ref tempCoordY);
            Properties.Settings.Default.InfoBoxTitleY = tempCoordY;

            isValid &= SaveCoordToSetting(ref Y_DateImage, ref tempCoordY);
            Properties.Settings.Default.DateImageY = tempCoordY;

            isValid &= SaveCoordToSetting(ref Y_DateText, ref tempCoordY);
            Properties.Settings.Default.DateTextY = tempCoordY;

            isValid &= SaveCoordToSetting(ref Y_TimeImage, ref tempCoordY);
            Properties.Settings.Default.TimeToBeatImageY = tempCoordY;

            isValid &= SaveCoordToSetting(ref Y_TimeText, ref tempCoordY);
            Properties.Settings.Default.TimeToBeatTextY = tempCoordY;

            isValid &= SaveCoordToSetting(ref Y_TrackImage, ref tempCoordY);
            Properties.Settings.Default.TrackImageY = tempCoordY;

            isValid &= SaveCoordToSetting(ref Y_TrackText, ref tempCoordY);
            Properties.Settings.Default.TrackTextY = tempCoordY;

            isValid &= SaveCoordToSetting(ref Y_LightCondImage, ref tempCoordY);
            Properties.Settings.Default.LightCondImageY = tempCoordY;

            isValid &= SaveCoordToSetting(ref HMargin_BottomArea, ref tempCoordX);
            Properties.Settings.Default.BikeTextsMargin = tempCoordX;
           
            isValid &= SaveCoordToSetting(ref Y_Constructor, ref tempCoordY);
            Properties.Settings.Default.ConstructorY = tempCoordY;

            isValid &= SaveCoordToSetting(ref Y_BikeDetails, ref tempCoordY);
            Properties.Settings.Default.BikeY = tempCoordY;


            if (isValid) { Properties.Settings.Default.Save(); }
            return isValid;
        }

        private bool SaveCoordToSetting(ref System.Windows.Controls.TextBox textBox, ref int settingValue)
        {
            if (Int32.TryParse(textBox.Text, out int i))
            {
                settingValue = i;
                return true;
            }
            settingValue = 0;
            return false;
        }
        
        private void SetUIConfiguration()
        {
            ExcelPicker.SetPath(Properties.Settings.Default.ExcelFilePath);
            InputFolderPicker.SetPath(Properties.Settings.Default.InputFolderPath);
            OutputFolderPicker.SetPath(Properties.Settings.Default.OutputFolderPath);

            // Fonts
            InfoBoxTitleTextConfig.Font = Properties.Settings.Default.InfoBoxTitleFont;
            InfoBoxTitleTextConfig.SetCBFontSize(InfoBoxTitleTextConfig.Font.Size);

            InfoBoxCommonTextConfig.Font = Properties.Settings.Default.InfoBoxCommonFont;
            InfoBoxCommonTextConfig.SetCBFontSize(InfoBoxCommonTextConfig.Font.Size);

            ConstructorTextConfig.Font = Properties.Settings.Default.ConstructorFont;
            ConstructorTextConfig.SetCBFontSize(ConstructorTextConfig.Font.Size);

            BikeTextConfig.Font = Properties.Settings.Default.BikeFont;
            BikeTextConfig.SetCBFontSize(BikeTextConfig.Font.Size);


            // Colors
            InfoBoxTitleTextConfig.SetColorARGB(Properties.Settings.Default.InfoBoxTitleColor);
            InfoBoxCommonTextConfig.SetColorARGB(Properties.Settings.Default.InfoBoxCommonColor);
            ConstructorTextConfig.SetColorARGB(Properties.Settings.Default.ConstructorColor);
            BikeTextConfig.SetColorARGB(Properties.Settings.Default.BikeColor);

            // Uppercase
            InfoBoxTitleTextConfig.SetUpperCase(Properties.Settings.Default.InfoBoxTitleUpper);
            InfoBoxCommonTextConfig.SetUpperCase(Properties.Settings.Default.InfoBoxCommonUpper);
            ConstructorTextConfig.SetUpperCase(Properties.Settings.Default.ConstructorUpper);
            BikeTextConfig.SetUpperCase(Properties.Settings.Default.BikeUpper);

            // Coordinates
            X_InfoBox.Text = Properties.Settings.Default.InfoBoxPos.X.ToString();
            Y_InfoBox.Text = Properties.Settings.Default.InfoBoxPos.Y.ToString();
            HMargin_InfoBox.Text = Properties.Settings.Default.InfoBoxTextsMargin.ToString();
            Y_Title.Text = Properties.Settings.Default.InfoBoxTitleY.ToString();
            
            Y_DateImage.Text = Properties.Settings.Default.DateImageY.ToString();
            Y_DateText.Text = Properties.Settings.Default.DateTextY.ToString();

            Y_TimeImage.Text = Properties.Settings.Default.TimeToBeatImageY.ToString();
            Y_TimeText.Text = Properties.Settings.Default.TimeToBeatTextY.ToString();

            Y_TrackImage.Text = Properties.Settings.Default.TrackImageY.ToString();
            Y_TrackText.Text = Properties.Settings.Default.TrackTextY.ToString();

            Y_LightCondImage.Text = Properties.Settings.Default.LightCondImageY.ToString();

            HMargin_BottomArea.Text = Properties.Settings.Default.BikeTextsMargin.ToString();
            Y_Constructor.Text = Properties.Settings.Default.ConstructorY.ToString();
            Y_BikeDetails.Text = Properties.Settings.Default.BikeY.ToString();
        }

        private bool LoadData()
        {
            excelFilePath = Properties.Settings.Default.ExcelFilePath;
            excelDataList.Clear();
            try
            {
                excelDataList = ExcelReader.Read(excelFilePath, ConsoleText);
            }
            catch (Exception)
            {
                bkgTaskWorking = false;
                return false;
            }

            inputFolderPath = Properties.Settings.Default.InputFolderPath;
            outputFolderPath = Properties.Settings.Default.OutputFolderPath;

            if (bkgTaskWorking)
            {
                if (Directory.Exists(inputFolderPath))
                {
                    // Load images to maps (not Backgrounds because are too big and used only once)
                    infoBoxImgMap.Clear();
                    LoadImagesFromFolder($"{inputFolderPath}\\{infoBoxFolderName}", infoBoxImgMap);

                    trackImgMap.Clear();
                    LoadImagesFromFolder($"{inputFolderPath}\\{trackFolderName}", trackImgMap);

                    lightConditionImgMap.Clear();
                    LoadImagesFromFolder($"{inputFolderPath}\\{lightCondFolderName}", lightConditionImgMap);

                    dateImage?.Dispose();
                    dateImage = null;
                    string dateImgPath = $"{inputFolderPath}\\{infoBoxFolderName}\\Date";
                    if (!FindImageWithAnyExtension(ref dateImage, dateImgPath))
                    {
                        dateImage = new Bitmap(bugImage, new System.Drawing.Size(50, 50));
                        ConsoleText.AppendText(String.Format("{0}ERROR: Date image not found in InputFolder\\{1}\\Date.(ext)!", Environment.NewLine, infoBoxFolderName));
                        ConsoleText.ScrollToEnd();
                    }

                    timeImage?.Dispose();
                    timeImage = null;
                    string timeImgPath = $"{inputFolderPath}\\{infoBoxFolderName}\\Time";
                    if (!FindImageWithAnyExtension(ref timeImage, timeImgPath))
                    {
                        timeImage = new Bitmap(bugImage, new System.Drawing.Size(50,50));
                        ConsoleText.AppendText(String.Format("{0}ERROR: Time image not found in InputFolder\\{1}\\Time.(ext)!", Environment.NewLine, infoBoxFolderName));
                        ConsoleText.ScrollToEnd();
                    }
                }
                else
                {
                    ConsoleText.AppendText(String.Format("{0}ERROR: invalid input folder! Please check and retry.", Environment.NewLine, infoBoxFolderName));
                    ConsoleText.ScrollToEnd();
                    return false;
                }
            }
            else
            {
                return false;
            }

            // InfoBox title
            infoBoxTitleParams.font = Properties.Settings.Default.InfoBoxTitleFont;
            infoBoxTitleParams.brush = new SolidBrush(Properties.Settings.Default.InfoBoxTitleColor);
            infoBoxTitleParams.format = centerFormat;
            isInfoBoxTitleUppercase = Properties.Settings.Default.InfoBoxTitleUpper;

            // InfoBox common
            infoBoxCommonParams.font = Properties.Settings.Default.InfoBoxCommonFont;
            infoBoxCommonParams.brush = new SolidBrush(Properties.Settings.Default.InfoBoxCommonColor);
            infoBoxCommonParams.format = centerFormat;
            isInfoBoxCommonUppercase = Properties.Settings.Default.InfoBoxCommonUpper;

            // Bike constructor
            bikeConstructorParams.font = Properties.Settings.Default.ConstructorFont;
            bikeConstructorParams.brush = new SolidBrush(Properties.Settings.Default.ConstructorColor);
            bikeConstructorParams.format = centerFormat;
            isBikeConstructorUppercase = Properties.Settings.Default.ConstructorUpper;

            // Bike details
            bikeDetailsParams.font = Properties.Settings.Default.BikeFont;
            bikeDetailsParams.brush = new SolidBrush(Properties.Settings.Default.BikeColor);
            bikeDetailsParams.format = centerFormat;
            isBikeDetailsUppercase = Properties.Settings.Default.BikeUpper;

            // Texts margin
            infoBoxTextsMargin = Properties.Settings.Default.InfoBoxTextsMargin;
            bikeTextsMargin = Properties.Settings.Default.BikeTextsMargin;

            return true;
        }

        private bool LoadImagesFromFolder(string inputFolderPath, IDictionary<string, Image> map)
        {
            bool loadingOk = true;
            DirectoryInfo inputFolderInfo = new DirectoryInfo(inputFolderPath);
            try
            {
                FileInfo[] fileInfoList = inputFolderInfo.GetFiles("*.*");

                foreach (FileInfo fileInfo in fileInfoList)
                {
                    if (!bkgTaskWorking)
                    {
                        break;
                    }

                    string extension = fileInfo.Extension;
                    if (inputImgExtList.Contains(fileInfo.Extension))
                    {
                        Image img = Image.FromFile(fileInfo.FullName);
                        map.Add(Path.GetFileNameWithoutExtension(fileInfo.Name).Trim(' '), img);
                    }
                }

                ConsoleText.AppendText(String.Format("{0}Loaded {1} images from {2}.", Environment.NewLine, map.Count, inputFolderPath));
                ConsoleText.ScrollToEnd();
            }
            catch (DirectoryNotFoundException)
            {
                ConsoleText.AppendText(String.Format("{0}ERROR: {1} folder doesn't exist!", Environment.NewLine, inputFolderPath));
                ConsoleText.ScrollToEnd();
                loadingOk = false;
                return loadingOk;
            }
            catch (Exception)
            { }

            return loadingOk;
        }

        private void CreateWCPoster(Bitmap bmp, WCExcelData excelData)
        {
            Graphics g = Graphics.FromImage(bmp);

            // Position of Upper Left corner
            System.Drawing.Point infoBoxPosUL = Properties.Settings.Default.InfoBoxPos;

            // ---------------------------------- PRINT ----------------------------------

            string wcNumber = excelData.number.ToString();

            // Info Box
            int digitNumber = wcNumber.Length;
            Image infoBoxImage = null; 
            if (infoBoxImgMap.TryGetValue(digitNumber.ToString(), out Image infoBoxImg))
            {
                infoBoxImage = infoBoxImg;
            }
            else
            {
                infoBoxImage = new Bitmap(bugImage, new System.Drawing.Size(250, 600));
                ConsoleText.Dispatcher.Invoke(() =>
                {
                    ConsoleText.AppendText(String.Format("{0}ERROR: image for InfoBox with {1} digits not found!", Environment.NewLine, digitNumber));
                    ConsoleText.ScrollToEnd();
                });
            }

            g.DrawImage(infoBoxImage, new Rectangle(infoBoxPosUL.X, infoBoxPosUL.Y, infoBoxImage.Width, infoBoxImage.Height));

            // Position of Bottom Right corner
            System.Drawing.Point infoBoxPosBR = new System.Drawing.Point(infoBoxPosUL.X + infoBoxImage.Width, infoBoxPosUL.Y + infoBoxImage.Height);

            // Title
            string titleText = String.Format("Weekly{0}Challenge #{1}", Environment.NewLine, wcNumber);
            if (isInfoBoxTitleUppercase)
            {
                titleText = titleText.ToUpper();
            }

            System.Drawing.Point currentTextPosUL = new System.Drawing.Point(infoBoxPosUL.X + infoBoxTextsMargin, infoBoxPosUL.Y + Properties.Settings.Default.InfoBoxTitleY);
            System.Drawing.Point currentTextPosBR = new System.Drawing.Point(infoBoxPosBR.X - infoBoxTextsMargin, infoBoxPosBR.Y);
            infoBoxTitleParams.rectf = TextPrinterParams.CalculateRect(currentTextPosUL, currentTextPosBR);
            TextPrinter.PrintText(titleText, g, infoBoxTitleParams);

            // Date interval
            int dateImageX = infoBoxPosUL.X + ((infoBoxImage.Width - dateImage.Width) / 2);
            g.DrawImage(dateImage, new Rectangle(dateImageX, infoBoxPosUL.Y + Properties.Settings.Default.DateImageY, dateImage.Width, dateImage.Height));

            string dateText = String.Format("{0}{1}{2}", excelData.startDate, dateSeparator, excelData.endDate);
            if (isInfoBoxCommonUppercase)
            {
                dateText = dateText.ToUpper();
            }
            currentTextPosUL.Y = infoBoxPosUL.Y + Properties.Settings.Default.DateTextY;
            TextPrinterParams dateParams = new TextPrinterParams(infoBoxCommonParams)
            {
                rectf = TextPrinterParams.CalculateRect(currentTextPosUL, currentTextPosBR)
            };

            TextPrinter.PrintText(dateText, g, dateParams);

            // Time to beat
            int timeImageX = infoBoxPosUL.X + ((infoBoxImage.Width - timeImage.Width) / 2);
            g.DrawImage(timeImage, new Rectangle(timeImageX, infoBoxPosUL.Y + Properties.Settings.Default.TimeToBeatImageY, timeImage.Width, timeImage.Height));

            string timeText = excelData.timeToBeat;
            if (isInfoBoxCommonUppercase)
            {
                timeText = timeText.ToUpper();
            }
            currentTextPosUL.Y = infoBoxPosUL.Y + Properties.Settings.Default.TimeToBeatTextY;
            TextPrinterParams timeParams = new TextPrinterParams(infoBoxCommonParams)
            {
                rectf = TextPrinterParams.CalculateRect(currentTextPosUL, currentTextPosBR)
            };
            TextPrinter.PrintText(timeText, g, timeParams);

            // Track
            string trackText = excelData.track;
            Image trackImage = null;
            if (trackImgMap.TryGetValue(trackText, out Image trackImg))
            {
                trackImage = trackImg;
            }
            else
            {
                trackImage = new Bitmap(bugImage, new System.Drawing.Size(72, 72));
                ConsoleText.Dispatcher.Invoke(() =>
                {
                    ConsoleText.AppendText(String.Format("{0}ERROR: image for track \"{1}\" not found!", Environment.NewLine, trackText));
                    ConsoleText.ScrollToEnd();
                });
            }
            int trackImageX = infoBoxPosUL.X + ((infoBoxImage.Width - trackImage.Width) / 2);
            g.DrawImage(trackImage, new Rectangle(trackImageX, infoBoxPosUL.Y + Properties.Settings.Default.TrackImageY, trackImage.Width, trackImage.Height));

            if (isInfoBoxCommonUppercase)
            {
                trackText = trackText.ToUpper();
            }
            currentTextPosUL.Y = infoBoxPosUL.Y + Properties.Settings.Default.TrackTextY;
            TextPrinterParams trackParams = new TextPrinterParams(infoBoxCommonParams)
            {
                rectf = TextPrinterParams.CalculateRect(currentTextPosUL, currentTextPosBR)
            };
            TextPrinter.PrintText(trackText, g, trackParams);

            // Light condition
            Image lightCondImage = null;
            if (lightConditionImgMap.TryGetValue(excelData.lightCondition, out Image lightCondImg))
            {
                lightCondImage = lightCondImg;
            }
            else
            {
                lightCondImage = new Bitmap(bugImage, new System.Drawing.Size(52, 52));
                ConsoleText.Dispatcher.Invoke(() =>
                {
                    ConsoleText.AppendText(String.Format("{0}ERROR: image for light condition \"{1}\" not found!", Environment.NewLine, excelData.lightCondition));
                    ConsoleText.ScrollToEnd();
                });
            }
            int lightCondImageX = infoBoxPosUL.X + ((infoBoxImage.Width - lightCondImage.Width) / 2);
            g.DrawImage(lightCondImage, new Rectangle(lightCondImageX, infoBoxPosUL.Y + Properties.Settings.Default.LightCondImageY, lightCondImage.Width, lightCondImage.Height));

            // Bike constructor
            string bikeConstructorText = excelData.bikeConstructor;
            if (isBikeConstructorUppercase)
            {
                bikeConstructorText = bikeConstructorText.ToUpper();
            }

            if ((bmp.Width - infoBoxPosBR.X) < infoBoxPosUL.X)    // Bike texts on the left, InfoBox on the right
            {
                currentTextPosUL.X = bikeTextsMargin;
                currentTextPosBR.X = infoBoxPosUL.X - 1;
            }
            else   // InfoBox on the left, Bike texts on the right
            {
                currentTextPosUL.X = infoBoxPosBR.X + 1 + bikeTextsMargin;
                currentTextPosBR.X = bmp.Width - bikeTextsMargin;
            }
            currentTextPosUL.Y = Properties.Settings.Default.ConstructorY;
            currentTextPosBR.Y = infoBoxPosBR.Y;
            bikeConstructorParams.rectf = TextPrinterParams.CalculateRect(currentTextPosUL, currentTextPosBR);
            TextPrinter.PrintText(bikeConstructorText, g, bikeConstructorParams);

            // Bike details
            string bikeDetails = String.Format("{0} ({1})", excelData.bikeModel, excelData.bikeYear.ToString());
            if (isBikeDetailsUppercase)
            {
                bikeDetails = bikeDetails.ToUpper();
            }
            currentTextPosUL.Y = Properties.Settings.Default.BikeY;
            bikeDetailsParams.rectf = TextPrinterParams.CalculateRect(currentTextPosUL, currentTextPosBR);
            TextPrinter.PrintText(bikeDetails, g, bikeDetailsParams);

            // Flush all graphics changes to the bitmap
            g.Flush();
            g.Dispose();

            // ---------------------------------- SAVE ----------------------------------
            Directory.CreateDirectory(Properties.Settings.Default.OutputFolderPath);
            string outImagePath = String.Format("{0}\\{1}.png", Properties.Settings.Default.OutputFolderPath, wcNumber);
            bmp.Save(outImagePath);

            // ------------------------------- UPDATE UI --------------------------------
            BitmapSource bitmapSource = CreateBitmapSourceFromGdiBitmap(bmp);
            bitmapSource.Freeze();
            outImg.Dispatcher.Invoke(() =>
            {
                outImg.Source = null;
                // UI operation goes inside of Invoke
                outImg.Source = bitmapSource;
            });

        }

        private static BitmapSource CreateBitmapSourceFromGdiBitmap(Bitmap bitmap)
        {
            if (bitmap == null)
                throw new ArgumentNullException("bitmap");

            var rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);

            var bitmapData = bitmap.LockBits(
                rect,
                ImageLockMode.ReadWrite,
                PixelFormat.Format32bppArgb);

            try
            {
                var size = (rect.Width * rect.Height) * 4;

                return BitmapSource.Create(
                    bitmap.Width,
                    bitmap.Height,
                    bitmap.HorizontalResolution,
                    bitmap.VerticalResolution,
                    System.Windows.Media.PixelFormats.Bgra32,
                    null,
                    bitmapData.Scan0,
                    size,
                    bitmapData.Stride);
            }
            finally
            {
                bitmap.UnlockBits(bitmapData);
            }
        }
    }
}
