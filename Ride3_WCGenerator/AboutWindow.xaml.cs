﻿using System.Windows;

namespace Ride3_WCGenerator
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (Visibility == Visibility.Hidden)
            {
                base.OnClosing(e);
            }
            else
            {
                e.Cancel = true;  // Cancels the window close    
                this.Hide();      // Programmatically hides the window
            }
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.ToString());
        }
    }
}
