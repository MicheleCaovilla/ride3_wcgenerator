﻿using System;
using System.Collections.Generic;
using System.IO;

using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace Ride3_WCGenerator
{
    struct WCExcelData
    {
        public readonly int number, bikeYear;
        public readonly string startDate, endDate, timeToBeat, track, lightCondition, bikeConstructor, bikeModel;

        public WCExcelData(int number, string startDate, string endDate, string timeToBeat, string track,
                            string lightCondition, string bikeConstructor, string bikeModel, int bikeYear)
        {
            this.number = number;
            this.startDate = startDate;
            this.endDate = endDate;
            this.timeToBeat = timeToBeat;
            this.track = track;
            this.lightCondition = lightCondition;
            this.bikeConstructor = bikeConstructor;
            this.bikeModel = bikeModel;
            this.bikeYear = bikeYear;
        }
    }

    class ExcelReader
    {
        public ExcelReader() { }

        static readonly string dateFormat = "dd/MM";
        static readonly DataFormatter dataFormatter = new DataFormatter(System.Globalization.CultureInfo.InvariantCulture);

        static readonly int numberIndex          = 0;
        static readonly int skipIndex            = 1;
        static readonly int startDateIndex       = 2;
        static readonly int endDateIndex         = 3;
        static readonly int timeToBeatIndex      = 4;
        static readonly int trackIndex           = 5;
        static readonly int lightConditionIndex  = 6;
        static readonly int bikeConstructorIndex = 7;
        static readonly int bikeModelIndex       = 8;
        static readonly int bikeYearIndex        = 9;

        public static List<WCExcelData> Read(string excelFilePath, System.Windows.Controls.TextBox ConsoleText)
        {
            List<WCExcelData> excelDataList = new List<WCExcelData>();

            IWorkbook workbook;
            try
            {
                using (FileStream fs = new FileStream(excelFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    if (excelFilePath.EndsWith(".xls"))
                    {
                        workbook = new HSSFWorkbook(fs);
                    }
                    else
                    {
                        workbook = new XSSFWorkbook(fs);
                    }
                }
            }
            catch (Exception e)
            {
                ConsoleText.AppendText(String.Format("{0}ERROR opening excel file, exception is \"{1}\"",
                                        Environment.NewLine, e.ToString()));
                ConsoleText.ScrollToEnd();
                throw (e);
            }

            ISheet sheet = workbook.GetSheetAt(0);
            ICell numberCell, skipCell;
            IRow row;
            int rowIndex = 1;
            bool bBlankRowReached = false;

            while (!bBlankRowReached)
            {
                row = sheet.GetRow(rowIndex);
                if (row != null)
                {
                    numberCell = row.GetCell(numberIndex, MissingCellPolicy.RETURN_BLANK_AS_NULL);
                    skipCell = row.GetCell(skipIndex, MissingCellPolicy.RETURN_BLANK_AS_NULL);

                    if (numberCell != null)
                    {
                        // If Skip cell is empty, we should read the row
                        if (skipCell == null)
                        {
                            DateTime startDateTime = row.GetCell(startDateIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).DateCellValue;
                            DateTime endDateTime = row.GetCell(endDateIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).DateCellValue;

                            WCExcelData excelData = new WCExcelData(
                                (int) numberCell.NumericCellValue,
                                startDateTime.ToString(dateFormat),
                                endDateTime.ToString(dateFormat),
                                row.GetCell(timeToBeatIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim(' '),
                                row.GetCell(trackIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim(' '),
                                row.GetCell(lightConditionIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim(' '),
                                row.GetCell(bikeConstructorIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).StringCellValue.Trim(' '),
                                dataFormatter.FormatCellValue(row.GetCell(bikeModelIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK)).Trim(' '), // some bike models could be only numeric
                                (int) row.GetCell(bikeYearIndex, MissingCellPolicy.CREATE_NULL_AS_BLANK).NumericCellValue
                                );

                            excelDataList.Add(excelData);
                        }
                        else
                        {
                            ConsoleText.AppendText(String.Format("{0}Skip excel line {1} referencing Weekly Challenge number {2}",
                                        Environment.NewLine, (1 + rowIndex), (int) numberCell.NumericCellValue));
                            ConsoleText.ScrollToEnd();
                        }

                        ++rowIndex;
                    }
                    else
                    {
                        bBlankRowReached = true;
                    }
                }
                else
                {
                    bBlankRowReached = true;
                }
            }

            return excelDataList;
        }
        
    }

}
