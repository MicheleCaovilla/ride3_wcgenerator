﻿using System;
using System.Windows;
using System.Windows.Controls;
using Microsoft.WindowsAPICodePack.Dialogs;


namespace Ride3_WCGenerator
{
    /// <summary>
    /// Interaction logic for FilePicker.xaml
    /// </summary>
    public partial class FilePicker : UserControl
    {
        private CommonOpenFileDialog openFileDialog;
        private static readonly CommonFileDialogFilter excelFilter = new CommonFileDialogFilter("Excel files", "*.xls; *.xlsx");

        public bool IsFolderPicker
        {
            get { return (bool)GetValue(IsFolderPickerProperty); }
            set { SetValue(IsFolderPickerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsFolderPicker.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsFolderPickerProperty =
            DependencyProperty.Register("IsFolderPicker", typeof(bool), typeof(FilePicker), new PropertyMetadata(false));


        public FilePicker()
        {
            InitializeComponent();
        }

        public string GetPath()
        {
            return FPTextBox.Text;
        }

        public void SetPath(string path)
        {
            FPTextBox.Text = path;
            FocusToTheRight();
        }

        private void FocusToTheRight()
        {
            // The text box needs to have the focus for Select to work
            FPTextBox.Focus();
            // Move the caret to the end of the text box
            FPTextBox.Select(FPTextBox.Text.Length, 0);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            openFileDialog = new CommonOpenFileDialog
            {
                IsFolderPicker = IsFolderPicker
            };

            if (!IsFolderPicker)
            {
                // Set filter for file extension and default file 
                openFileDialog.Filters.Add(excelFilter);

                // openFileDialog.DefaultExtension = ".xls"; 
                // Not useful, it substitutes the file extension with the default
            }

            // Display OpenFileDialog by calling ShowDialog method 
            CommonFileDialogResult result = openFileDialog.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == CommonFileDialogResult.Ok)
            {
                // Open document 
                FPTextBox.Text = openFileDialog.FileName;
                FocusToTheRight();
            }
        }
    }
}
