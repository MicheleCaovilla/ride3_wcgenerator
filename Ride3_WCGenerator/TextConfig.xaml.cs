﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;

namespace Ride3_WCGenerator
{
    /// <summary>
    /// Interaction logic for TextConfig.xaml
    /// </summary>
    public partial class TextConfig : System.Windows.Controls.UserControl
    {
        private static string fontName;

        public Font Font { get; set; }

        private Color color;

        private static readonly List<float> CommonlyUsedFontSizes = new List<float>
        {
            8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f,
            15.0f, 16.0f, 17.0f, 18.0f, 19.0f,
            20.0f, 22.0f, 24.0f, 26.0f, 28.0f, 30.0f, 32.0f, 34.0f, 36.0f, 38.0f,
            40.0f, 44.0f, 48.0f, 52.0f, 56.0f, 60.0f, 64.0f, 68.0f, 72.0f, 76.0f,
            80.0f, 88.0f, 96.0f, 104.0f, 112.0f, 120.0f, 128.0f, 136.0f, 144.0f
        };

        public TextConfig()
        {
            InitializeComponent();

            if (Font == null)
            {
                Font = new Font("Arial", 10.0f, System.Drawing.FontStyle.Regular);
            }

            if (color == null)
            {
                color = Color.Black;
            }

            CB_FontSize.ItemsSource = CommonlyUsedFontSizes;
            SetCBFontSize(Font.Size);

            InitTextBox();
            InitColorBtn();
        }

        #region FONT

        private void FontsBtn_Click(object sender, RoutedEventArgs e)
        {
            FontDialog fontDialog = new FontDialog
            {
                Font = Font
            };

            DialogResult dialogResult = fontDialog.ShowDialog();
            if (dialogResult != DialogResult.Cancel)
            {
                Font = fontDialog.Font;
                SetCBFontSize((float)System.Math.Round(Font.Size));
                InitTextBox();
            }
        }

        public void SetCBFontSize(float newSize)
        {
            CB_FontSize.SelectedIndex = CommonlyUsedFontSizes.IndexOf(newSize);
        }

        private void FontSize_Selected(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            float newSize = float.Parse(CB_FontSize.SelectedValue.ToString());
            if (Font.Size != newSize)
            {
                Font = new Font(Font.FontFamily, newSize, Font.Style);
            }
        }

        private void InitTextBox()
        {
            if (fontTextBox != null)
            {
                fontTextBox.FontFamily = new System.Windows.Media.FontFamily(Font.Name);
                fontTextBox.FontWeight = Font.Bold ? FontWeights.Bold : FontWeights.Regular;
                fontTextBox.FontStyle = Font.Italic ? FontStyles.Italic : FontStyles.Normal;
                if ((bool)CB_Uppercase.IsChecked)
                {
                    fontName = Font.Name.ToUpper();
                }
                else
                {
                    fontName = Font.Name;
                }
                fontTextBox.Text = fontName;
            }
        }

        #endregion FONT


        #region COLOR

        public Color GetColor()
        {
            return color;
        }

        public void SetColorARGB(Color colorARGB)
        {
            color = colorARGB;
            if (ColorAlpha != null)
            {
                ColorAlpha.Value = color.A;
            }
            InitColorBtn();
        }

        private void InitColorBtn()
        {
            if (colorBtn != null)
            {
                colorBtn.Background = new System.Windows.Media.SolidColorBrush(
                        System.Windows.Media.Color.FromArgb(color.A, color.R, color.G, color.B));
                colorBtn.BorderBrush = new System.Windows.Media.SolidColorBrush(
                        System.Windows.Media.Color.FromArgb(255, color.R, color.G, color.B));
            }
        }


        private void ColorBtn_Click(object sender, RoutedEventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog
            {
                Color = color,
                FullOpen = true
            };

            DialogResult dialogResult = colorDialog.ShowDialog();
            if (dialogResult != DialogResult.Cancel)
            {
                SetColorARGB(Color.FromArgb(GetAlpha(), colorDialog.Color.R, colorDialog.Color.G, colorDialog.Color.B));
            }
        }

        private int GetAlpha()
        {
            if (ColorAlpha != null && ColorAlpha.Value != null)
            {
                int alpha = (int)ColorAlpha.Value;

                if (alpha < 0)
                {
                    alpha = 0;
                }
                else if (alpha > 255)
                {
                    alpha = 255;
                }
                return alpha;
            }
            else
            {
                return 255;
            }
        }


        private void ColorAlpha_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (ColorAlpha.Value != null)
            {
                SetColorARGB(Color.FromArgb(GetAlpha(), color.R, color.G, color.B));
            }
        }

        #endregion COLOR


        #region Uppercase

        public bool GetUpperCase()
        {
            return (bool)CB_Uppercase.IsChecked;
        }

        public void SetUpperCase(bool isChecked)
        {
            CB_Uppercase.IsChecked = isChecked;
            InitTextBox();
        }

        private void UC_checked(object sender, RoutedEventArgs e)
        {
            fontName = Font.Name;
            fontTextBox.Text = fontName.ToUpper();
        }

        private void UC_unchecked(object sender, RoutedEventArgs e)
        {
            fontName = Font.Name;
            fontTextBox.Text = fontName;
        }
        #endregion Uppercase

    }
}
