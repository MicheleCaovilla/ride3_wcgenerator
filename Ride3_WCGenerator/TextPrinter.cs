﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Drawing2D;

namespace Ride3_WCGenerator
{
    struct TextPrinterParams
    {
        public Font font;
        public Brush brush;
        public RectangleF rectf;

        // String formatting options (used for alignment)
        public StringFormat format;

        public void Dispose()
        {
            font?.Dispose();
            font = null;
            brush?.Dispose();
            brush = null;
        }


        public TextPrinterParams(Font font, Brush brush, RectangleF rectf, StringFormat format)
        {
            this.font = font;
            this.brush = brush;
            this.rectf = rectf;
            this.format = format;
        }

        public TextPrinterParams(TextPrinterParams other)
        {
            font = other.font;
            brush = other.brush;
            rectf = other.rectf;
            format = other.format;
        }

        public TextPrinterParams(Font font, Brush brush, Point point1, Point point2, StringFormat stringFormat)
        {
            this.font = font;
            this.brush = brush;

            int width = Math.Abs(point2.X - point1.X);
            int height = Math.Abs(point2.Y - point1.Y);

            int xTopLeft = (point1.X <= point2.X) ? point1.X : point2.X;
            int yTopLeft = (point1.Y <= point2.Y) ? point1.Y : point2.Y;

            this.rectf = new RectangleF(xTopLeft, yTopLeft, width, height);
            this.format = stringFormat;
        }

        public static RectangleF CalculateRect(Point point1, Point point2)
        {
            int width = Math.Abs(point2.X - point1.X);
            int height = Math.Abs(point2.Y - point1.Y);

            int xTopLeft = (point1.X <= point2.X) ? point1.X : point2.X;
            int yTopLeft = (point1.Y <= point2.Y) ? point1.Y : point2.Y;

            return new RectangleF(xTopLeft, yTopLeft, width, height);
        }
    }


    class TextPrinter
    {
        public static void PrintText(string text, Bitmap bmp, TextPrinterParams printerParams)
        {
            // Create graphic object that will draw onto the bitmap
            Graphics g = Graphics.FromImage(bmp);

            PrintText(text, g, printerParams);
        }

        public static void PrintText(string text, Graphics g, TextPrinterParams printerParams)
        {
            // ------------------------------------------
            // Ensure the best possible quality rendering
            // ------------------------------------------
            // The smoothing mode specifies whether lines, curves, and the edges of filled areas use smoothing (also called antialiasing). 
            // One exception is that path gradient brushes do not obey the smoothing mode. 
            // Areas filled using a PathGradientBrush are rendered the same way (aliased) regardless of the SmoothingMode property.
            g.SmoothingMode = SmoothingMode.AntiAlias;

            // The interpolation mode determines how intermediate values between two endpoints are calculated.
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            // Use this property to specify either higher quality, slower rendering, or lower quality, faster rendering of the contents of this Graphics object.
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;

            // This one is important
            g.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

            // Draw the text onto the image
            g.DrawString(text, printerParams.font, printerParams.brush, printerParams.rectf, printerParams.format);

            // Flush all graphics changes to the bitmap
            // g.Flush(); // moved outside to call it once
        }
    }
}
