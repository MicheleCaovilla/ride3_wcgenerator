# Introduction #

**The posters for Ride3 Weekly Challenges are a mess.**

Here is a list of the macroscopic errors:

* The title is too large and seems to touch the white border of the containing panel
* The date interval is often wrong, it varies from 6 to 8 days
* The date and time formats vary from one poster to another
* Track names with 2 words are often missing a space separator (because they are copied from the database key). In the first posters, they included also the subtrack part of the db key (e.g. "GardaLake_A")
* The track image is always referring to Ottobiano track
* Finally, the image description sometimes differs from the image contents

![Error example](https://bitbucket.org/MicheleCaovilla/ride3_wcgenerator/raw/d8ac2c01f6d88e1327160f26d48663d02dc28b16/screenshots/errors.png)

This is clearly an unprofessional and unpassionate way to work, that ruins all the efforts made by the other departments.

I created a **image generator** for Ride3 Weekly Challenges posters. It takes data from an Excel file and some image folders. It lets you create all the posters in one shot.

**This application is a declaration of love to the Ride brand and Milestone**, also if we got different "visions". I hope Milestone could try and enjoy this simple tool.

Anyway, I improved my skills learning some C# and WPF. I'm sure that there is space for improvement using some language specific features that I don't have study yet.

*Disclaimer: all images, logos, names, etc are the copyrighted property of Milestone and/or THQ Nordic.*

---

# How to #

## Installation ##
Get a software version from the *release* folder and unzip it. Then you can simply launch *Ride3 WC Generator.exe*.

You will need some data to use this application. In the *release* folder you can find an Excel file called *WeeklyChallenge data.xlsx* and the *Input* folder, which contains all the images you need (backgrounds, tracks, etc) with the correct folder structure. Download and put them where you like most. 

## Use ##

![Homepage](https://bitbucket.org/MicheleCaovilla/ride3_wcgenerator/raw/d8ac2c01f6d88e1327160f26d48663d02dc28b16/screenshots/r3wcg_home_v1.0.png)

As you can see, there are a lot of parameters to set up. After that, click on the start button to generate the posters, a preview of the output will appear on the right side. You can stop the process, if you want.
A textual log on the bottom side will help you in case of errors.

The origin of the coordinates system is the upper left corner. The Y of the fields inside the infobox (like track, date, etc) is referred to top border of the box. The X is not necessary because the graphics will be automatically centered. The horizontal margin is the minimum distance between the texts inside the box and the infobox border.

The Y of the bottom area fields is referred to top border of the entire poster. The horizontal margin is the minimum distance between the texts of the bottom area and the infobox border.

The size of the text is influenced by the screen resize setting in Windows 10 (and maybe other Windows versions), so the default settings could be wrong for your PC.

This is a possible output result. I don't know which is the original font used, so I choose the classic *Arial*.

![output](https://bitbucket.org/MicheleCaovilla/ride3_wcgenerator/raw/e6134d5e55eda33514db5cb74b208b7bd796d887/screenshots/output.png)

---

# Technology stack #

I used these free software to create Ride3 WC Generator and I would like to thank their creators.

* C# and WPF development on Visual Studio 2017 Community
* Repository managed using Sourcetree
* [NPOI](https://www.nuget.org/packages/NPOI/) to read Excel file
* [paint.net](https://www.getpaint.net/) for image elaboration
* [WPF Toolkit](https://github.com/xceedsoftware/wpftoolkit) 

